To use these settings in your IntelliJ IDEA config, do the following:

* Navigate to the IDEA config directory if it exists.  It will be in the .IntelliJIdea11 directory 
  in your user directory (on Windows, at least).
* Back up the current settings in this directory.
* Delete everything in this directory.
* Type 'git clone git@github.com:michael-taylor/IntelliJ-IDEA-Settings.git .' in this directory.